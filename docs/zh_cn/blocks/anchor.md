![滑车锚](block:betterwithmods:anchor)

滑车锚用于将绳索固定到位. 
如果放在木块的侧面或底部，可以用[麻绳]不断地敲击绳子(../items/rope.md) 尽可能地伸展一根可攀爬的绳子,可用空手拨动，从底部取出绳子.

如果放在 [平台]顶部 (platform.md) 或者 [铁墙](iron_wall.md) 滑车锚可以连接[滑车](pulley.md) 允许移动所有连接的平台.

![基本滑车和平台设置](betterwithmods:pulley1.png)