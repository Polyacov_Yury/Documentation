![风箱](block:betterwithmods:bellows)

风箱是一种气动叶片，当给定机械功率时，风箱会收缩并向其所面对的方向吹一阵风. 它不会收缩，这可以通过向下线某处的 [木质变速箱](wooden_gearbox.md) 发送红石信号来实现
风箱产生的阵风能够助燃火焰，尽管正常的火源不够强大，无法被吹出. 要解决这个问题，请使用 [火盆](hibachi.md).

另外，风箱可以吹到它前面的东西。不同重量的物品会走不同的距离.
 * 非常轻的物品，如灰尘、羽毛和纸张，将离风箱4个方格远.
   
 * 中等重量的物品，如种子、大麻制品和磨碎的网架将到3个方格.
   
 * 重的物品（或未知的项目）将到1个方格.  

