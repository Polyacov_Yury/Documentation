![蜡烛](block:betterwithmods:candle)

蜡烛是用 [动物油脂]和细绳制成的一种基本的美化方块(../items/tallow.md). 它只能放在方块的顶部，不能放在侧面. 搭配 [烛台] 看起来不错喔(candle_holders.md)