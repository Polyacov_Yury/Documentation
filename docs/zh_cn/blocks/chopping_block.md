![Chopping Block](block:betterwithmods:aesthetic)

斩首块主要是一个美观的块，当与[螺杆锯](saw.md) 组合使用时，会增加斩首暴徒的机会.
当一个暴徒死了。每当一个暴徒以这种方式死去，砧板就会变得血淋淋的，你可以用 [肥皂] 清洗它(../items/soap.md).