![挚友方块](block:betterwithmods:companion_cube)

伙伴立方体是通过将狼放在 [方块放置回收器](block_dispenser.md) 前面并激活它来创建的.

警告：在制造这个模型的过程中，只有少数数字动物受到伤害.