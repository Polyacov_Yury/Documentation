Decorative Blocks

![木桌](block:betterwithmods:wood_table@1)

桌子是装饰块，有一个坚实的顶面，下面有动态支撑梁.

![木凳](block:betterwithmods:wood_bench@1)

木凳和桌子在所有方面都是一样的，除了它只有半个方块高. 
![板条栅栏](block:betterwithmods:slats@1)

板条是一种装饰性的木块，由原版木制成，其作用类似于玻璃板

![网格栅栏](block:betterwithmods:grate@1)

 格栅是一种装饰性的木块，由原版木制成，其作用类似于玻璃板 

![筚板](block:betterwithmods:wicker)

 筚板是一种装饰性的木块. 其作用类似于玻璃板

![筚板块](block:betterwithmods:aesthetic@12)

筚板块是筚板的全块版本
