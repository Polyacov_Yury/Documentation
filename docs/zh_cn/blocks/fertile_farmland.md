![肥沃的田地](block:betterwithmods:fertile_farmland)

肥沃的农田被用来加速植物的生长，通过把骨粉放进耕地中制成.
除非你用的是 [培植皿]否则肥料会随机地耗尽植物，而且必须时刻准备好(planter.md)

[硬核版骨粉](../hardcore/index.md): 如果启用，肥沃的农田是唯一可以加速作物生长的方式. 


