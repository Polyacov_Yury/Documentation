![手摇曲柄](block:betterwithmods:hand_crank)

手摇曲柄是产生机械动力的最早形式，它可以为任何不需要连续机械动力的机器提供动力r.
例如， the [磨石](millstone.md) 将是第一台您希望使用手摇曲柄开始创建风车的机器.

