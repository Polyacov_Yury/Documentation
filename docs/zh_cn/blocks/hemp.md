![大麻](item:betterwithmods:material@2)
![大麻种子](block:betterwithmods:hemp)

大麻是一种强韧的纤维植物，可以用于许多不同的事物，包括强韧的[麻绳](../items/rope.md) or [Fabric](../items/fabric.md)

大麻种子，若启用了硬核版种子，只能用锄头在地上耕作有几率获得；若禁用硬核版种子，它们从高草上掉落.

这些种子必须种植在水分充足的农田上，并有充足的光线，无论是来自阳光还是来自[发光灯](light.md).
也可以通过[肥沃的田地](fertile_farmland.md) 加速增长.


大麻有两个阶段，最好让植物生长到第二阶段，只收获顶部的块，类似于甘蔗. 

![阶段一](betterwithmods:hemp-stage-1.png)
![阶段二](betterwithmods:hemp-stage-2.png)

