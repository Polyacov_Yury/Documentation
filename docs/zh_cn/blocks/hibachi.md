![火盆](block:betterwithmods:hibachi)

当给出红石信号时，火盆将创建一个连续的火源。只有火盆的火焰足够被 [风箱]助力(bellows.md).
 