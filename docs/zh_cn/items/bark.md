![树皮](item:betterwithmods:bark)

用斧子把原木做成木板，或者用 [螺杆锯](../blocks/saw.md)把木头锯下来，就可以获得木头上的树皮.
它可以用作熔炉燃料，也可以用作制[鞣制皮革](tanned_leather.md)的鞣料.